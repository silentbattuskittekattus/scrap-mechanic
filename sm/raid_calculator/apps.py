from django.apps import AppConfig


class RaidCalculatorConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'raid_calculator'
